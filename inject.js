var sidebarShown = false;
var downloadedAlbum = false;

function deferredAddZip(url, filename, zip) {
    var deferred = $.Deferred();
    JSZipUtils.getBinaryContent(url, function (err, data) {
        if (err) {
            deferred.reject(err);
        } else {
            zip.file(filename, data, {binary: true});
            deferred.resolve(data);
        }
    });
    return deferred;
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

var confirmPageChange = function (e) {
    e = e || window.event;
    var message = "We are still compressing the album.";
    if (e)
    {
        e.returnValue = message;
    }
    return message;
}

function downloadAlbum() {
    if (downloadedAlbum === true) {
        return;
    }
    window.onbeforeunload = confirmPageChange;
    var songCount = TralbumData.trackinfo.length;
    var zip = new JSZip();
    var deferreds = [];

    $(".dl-all-button").text("Downloading album, please wait.");

    deferreds.push(deferredAddZip(TralbumData.artFullsizeUrl, "AlbumArt.jpg", zip));

    for (i = 0; i <= (songCount - 1); i++) {
        var url = TralbumData.trackinfo[i].file["mp3-128"].replace("http://", "https://");
        var filename = "[" + pad(i + 1, 2) + "] " + TralbumData.trackinfo[i].title + ".mp3";
        console.log("Added track: " + TralbumData.trackinfo[i].title);
        deferreds.push(deferredAddZip(url, filename, zip));
    }

    $.when.apply($, deferreds).done(function () {
        var content = zip.generate({type: "blob"});

        $(".dl-all-button").text("Enjoy!");
        window.onbeforeunload = null;

        saveAs(content, TralbumData.artist + " - " + TralbumData.current.title + ".zip");
        downloadedAlbum = true;
    }).fail(function (err) {
        showError(err);
    });
}

$(document).ready(function () {
    var tList = TralbumData.trackinfo;

    var packageUrls = "";

    for (t in tList) {
        var item;

        item = '<li><a class="dl-link custom-color" href="' + tList[t].file["mp3-128"] + '" download="[' + tList[t].track_num + '] ' + BandData["name"] + ' - ' + tList[t].title + '.mp3">' + tList[t].title + '</a></li>';
        packageUrls = packageUrls + tList[t].file["mp3-128"] + "><";
        $(".dl-tracklist").append(item);
    }

    $(".dl-sidebar").css("background", $("#pgBd").css("background"));
    $(".dl-sidebar").css("color", $("#pgBd").css("color"));
    $(".dl-sidebar").css("border-color", $("#pgBd").css("color"));
    $(".dl-all-button").css("backgroundColor", $("#follow-unfollow").css("backgroundColor"));
    $(".dl-all-button").css("color", $("#follow-unfollow").css("color"));
    $(".dl-all-button").css("border", $("#follow-unfollow").css("border"));

    $(".dl-sidebar").animate({
        opacity: 1
    });

    $(window).bind('keydown', function (event) {
        if (event.ctrlKey === false) {
            switch (String.fromCharCode(event.which).toLowerCase()) {
                case 'd':
                    event.preventDefault();
                    toggleSidebar();
                    break;
            }
        }
    });

    function toggleSidebar() {
        if ($(".dl-sidebar").css("opacity") === "1") {
            if (sidebarShown === true) {
                $(".dl-sidebar").stop();
                $(".dl-sidebar-container").animate({
                    left: "-400px"
                });
                $(".dl-sidebar-toggle").animate({
                    left: "-1px"
                });
                sidebarShown = false;
            } else {
                $(".dl-sidebar").stop();
                $(".dl-sidebar-container").animate({
                    left: "0px"
                });
                $(".dl-sidebar-toggle").animate({
                    left: "399px"
                });
                sidebarShown = true;
            }
        }
    }

    $(".dl-sidebar-toggle").click(function () {
        toggleSidebar();
    });

    $(".dl-all-button").click(function () {
        downloadAlbum();
    });

    $(".dl-all-button").hover(function () {
        $(".dl-all-button").stop();
        $(".dl-all-button").animate({
            backgroundColor: $("#pgFt").css("backgroundColor")
        }, 200);
    }, function () {
        $(".dl-all-button").stop();
        $(".dl-all-button").animate({
            backgroundColor: $("#follow-unfollow").css("backgroundColor")
        }, 200);
    });
});