# README #

This extension adds a way to download Bandcamp songs. I do not support the theft of music, however if you do not have the money, it is alluring. If you like the music, make sure you support the artist by buying it. Many of them make a living via music.

### What is this repository for? ###

* BitBucket is used as a backup solution for all projects.
* Other developers can look into the source for potential ideas.

### How do I get set up? ###

* We use SourceTree to handle Git interfacing.
* We edit these files with NetBeans, due to the good support of Javascript, and auto-formatting.
* We use Google Chrome Canary for testing, as we can ensure the extension is compatible with future versions of Chrome/Chromium.

### Contribution guidelines ###

* Ensure the code works on multiple pages, for maximum compatibility.
* Ensure cache is off, so the files aren't loaded from memory, but from the proper source.

### Who do I talk to? ###

* The project manager is Aaesos, please contact him on any inquiries.