$.get(chrome.extension.getURL("html/downloader.html"), function (data) {
    $("body").append(data);
});

function injectJs(link) {
    var scr = document.createElement("script");
    scr.type = "text/javascript";
    scr.src = link;
    document.getElementsByTagName("head")[0].appendChild(scr);
}

injectJs(chrome.extension.getURL("js/jszip.js"));
injectJs(chrome.extension.getURL("js/jszip-utils.js"));
injectJs(chrome.extension.getURL("js/FileSaver.js"));
injectJs(chrome.extension.getURL("inject.js"));